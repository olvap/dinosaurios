import React from 'react';
import Card from './Card'
import './App.css';
import cards from './cards.json';

function App() {
  return (
    <div className="App">
    {
      cards.map((card, index) => {
        return (
          <Card key={index} name={card.name} />
        )
      })
    }
    </div>
  );
}

export default App;
